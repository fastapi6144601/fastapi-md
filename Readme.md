# Do this after git clone (Run the program with the command):
- python -m venv venv
- venv\Scripts\activate.bat (windows) / source venv/bin/activate (Mac/Linux)
- pip install -r requirements.txt
- Create a config folder containing the database.py file. The contents of the file are:

````
from pymongo.mongo_client import MongoClient

uri = "mongodb+srv://<username>:<password>@<server>/?retryWrites=true&w=majority"
client = MongoClient(uri)

db = client.todo_db
````

- uvicorn main:app --port 3000  --reload
