from fastapi import FastAPI
from routes.route_todo import route_todo
from routes.route_category import route_category

app = FastAPI()

app.include_router(route_todo)
app.include_router(route_category)