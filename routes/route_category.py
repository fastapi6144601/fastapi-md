import datetime

from bson import ObjectId
from fastapi import APIRouter
from models.model_category import Category
from config.database import collection_category
from schemas.schema_category import list_category

route_category = APIRouter()

# GET request method
@route_category.get("/category", tags=["category"])
async def get_categories():
  categories = list_category(collection_category.find())
  return categories

# POST request method
@route_category.post("/category", tags=["category"])
async def post_category(category: Category) -> dict:
  category = dict(category)
  category["created_at"] = datetime.datetime.now()
  category["updated_at"] = datetime.datetime.now()
  collection_category.insert_one(dict(category))
  return category

# PUT request method
@route_category.put("/category", tags=["category"])
async def put_category(id:str, category: Category) -> dict:
  category = dict(category)
  category["updated_at"] = datetime.datetime.now()
  collection_category.find_one_and_update({"_id": ObjectId(id)}, {"$set": dict(category)})
  return category

# DELETE request method
@route_category.delete("/category", tags=["category"])
async def delete_category(id:str) -> str:
  collection_category.find_one_and_delete({"_id": ObjectId(id)})
  return id + " was deleted"
