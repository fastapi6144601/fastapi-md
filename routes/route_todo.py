from fastapi import APIRouter
from models.model_todo import Todo
from config.database import collection_todo
from schemas.schema_todo import list_todo
from bson import ObjectId

route_todo = APIRouter()

# GET request method
@route_todo.get("/todo", tags=["todo"])
async def get_todos():
  todos = list_todo(collection_todo.find())
  return todos

# POST request method
@route_todo.post("/todo", tags=["todo"])
async def post_todo(todo: Todo) -> dict:
  collection_todo.insert_one(dict(todo))
  return dict(todo)

# PUT request method
@route_todo.put("/todo", tags=["todo"])
async def put_todo(id:str, todo: Todo) -> dict:
  collection_todo.find_one_and_update({"_id": ObjectId(id)}, {"$set": dict(todo)})
  return dict(todo)

# DELETE request method
@route_todo.delete("/todo", tags=["todo"])
async def delete_todo(id:str) -> str:
  collection_todo.find_one_and_delete({"_id": ObjectId(id)})
  return id + " was deleted"
