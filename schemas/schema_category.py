def single_category(category) -> dict:
  return {
    "id": str(category["_id"]),
    "category": category["name"],
    "created_at": str(category["created_at"]),
    "updated_at": str(category["updated_at"])
  }

def list_category(categories) -> list:
  return [single_category(category) for category in categories]